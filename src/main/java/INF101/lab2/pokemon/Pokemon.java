package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {

    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random;


    //implement constructor that takes String name as argument
    public Pokemon(String name) {
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));

    }

    public String getName() {
        //throw new UnsupportedOperationException("Not implemented yet.");
        //return name of pokemon
        
        return this.name;
    }


    @Override
    public int getStrength() {
        //throw new UnsupportedOperationException("Not implemented yet.");
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        //throw new UnsupportedOperationException("Not implemented yet.");
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
        //throw new UnsupportedOperationException("Not implemented yet.");
        return this.maxHealthPoints;
    }

    public boolean isAlive() {
        //throw new UnsupportedOperationException("Not implemented yet.");
        return this.healthPoints > 0;
    }

    @Override
    public void attack(IPokemon target) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        target.damage(damageInflicted);
        System.out.println(target + " attacks " + this.name + "\n");
        
    }

    @Override
    public void damage(int damageTaken) {
        //throw new UnsupportedOperationException("Not implemented yet.");
        if (damageTaken < 0) {
            damageTaken = 0;
        }
        if (this.healthPoints <= 0) {
            damageTaken = 0;
        }
        if (damageTaken > this.healthPoints) {
            this.healthPoints = 0;
            damageTaken = 0;
        }

        this.healthPoints -= damageTaken;
        System.out.println(this.name + " takes " + damageTaken + " damage and is left with " + this.healthPoints + this.maxHealthPoints + " HP");
    }

    @Override
    public String toString() {
        //throw new UnsupportedOperationException("Not implemented yet.");
        return this.name + " HP: " + "(" + this.healthPoints + "/" + this.maxHealthPoints + ")" + " STR: " + this.strength;
    }

}
