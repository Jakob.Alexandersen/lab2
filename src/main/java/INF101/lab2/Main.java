package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        // Have two pokemon fight until one is defeated
        Pokemon pokemon1 = new Pokemon("torger");
        Pokemon pokemon2 = new Pokemon("johannes");
        Main.pokemon1 = pokemon1;
        Main.pokemon2 = pokemon2;

        while (Main.pokemon1.isAlive() && Main.pokemon2.isAlive()) {
            Main.pokemon1.attack(Main.pokemon2);
            Main.pokemon2.attack(Main.pokemon1);
        }


    }
}
